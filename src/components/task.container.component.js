import { CreateComponent } from '../component.js'

export default class TaskContainer extends CreateComponent({
    template:   `<p id="header">{title}</p>`,

    selector: "task-container"
}) {
    constructor(category = "", title = "") {
        super();
        this.element.className = "drap-drop-container";

        this.category = category;
        this.title = title;
    }

    update() {
        super.update();
        this.element.setAttribute("task-category", this.category);
    }
}