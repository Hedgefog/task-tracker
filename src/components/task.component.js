import { Utils } from '../utils'
import { CreateComponent } from '../component.js'

export default class TaskComponent extends CreateComponent({
    template:   `<p id="title">{title}</p>
                <p id="description">{description}</p>
                <div class="priority" id="priority"></div>`,

    selector: "task"
}) {
     static get Category() {
        return {
            Pending: 0,
            ToDo: 1,
            InProgress: 2,
            Done: 3
        };
    }

    static get MinTitleLength() {
        return 4;
    }

    static get MinDescriptionLength() {
        return 4;
    }

    static get MaxTitleLength() {
        return 32;
    }

    static get MaxDescriptionLength() {
        return 128;
    }

    get category() {
        return this._category;
    }

    get history() {
        return this._history;
    }

    constructor(title = "", description = "", priority = 0) {
        super();
        this.element.className = "drap-draggable";

        this._category = null;
        this._history = [];
        this.title = title;
        this.id = Utils.generateUUID();
        this.description = description;
        this.priority = priority;

        this.moveTo(TaskComponent.Category.Pending);
    }

    update() {
        super.update();
        this.element.setAttribute("task-id", this.id);

        let priorityElement = this.element.querySelector("#priority");
        if (this.priority < 5) {
            priorityElement.classList.add("low");
        } else if (this.priority < 8) {
            priorityElement.classList.add("normal");
        } else {
            priorityElement.classList.add("high");
        }

        let titleElement = this.element.querySelector("#title");
        if (titleElement.innerText.length > TaskComponent.MaxTitleLength) {
            let text = titleElement.innerText.substring(0, TaskComponent.MaxTitleLength);
            titleElement.innerText = text + "...";
        }

        let descriptionElement = this.element.querySelector("#description");
        if (descriptionElement.innerText.length > TaskComponent.MaxDescriptionLength) {
            let text = descriptionElement.innerText.substring(0, TaskComponent.MaxDescriptionLength);
            descriptionElement.innerText = text + "...";
        }
    }

    moveTo(category) {
        if (this.category) {
            if ((this.category == TaskComponent.Category.Pending) && (category == TaskComponent.Category.Done)) {
                return false;
            } else if (this.category == TaskComponent.Category.Done) {
                return false;
            } else if (category == TaskComponent.Category.Pending) {
                return false;
            }
        }

        this._category = category;

        this._history.push({
            category: category,
            time: Date.now()
        });

        return true;
    }

    fromJson(json) {
        this.id = json.id;
        this.title = json.title;
        this.description = json.description,
        this._history = json.history;
        this._category = json.category;
        this.priority = json.priority;
    }

    toJson() {
        if (typeof(this.priority) == "string") {
            this.priority = parseInt(this.priority);
        }

        return {
            id: this.id,
            title: this.title,
            description: this.description,
            history: this._history,
            category: this._category,
            priority: this.priority
        };
    }
}