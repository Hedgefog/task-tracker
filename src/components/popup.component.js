import { CreateComponent } from '../component.js'

export default class PopUpComponent extends CreateComponent({
    template:   `<div id="title">{title}</div>
                <div id="message">{message}</div>
                <div id="buttons"></div>`,

    selector: "pop-up"
}) {
    constructor() {
        super();
        this.clear();

        document.body.appendChild(this.element);
        this.hide();
    }
    
    update() {
        super.update();

        let buttonsBlock = this.element.querySelector("#buttons");
        for (let i = 0; i < this.buttons.length; ++i) {
            const button = this.buttons[i];

            let buttonElement = document.createElement("div");
            buttonElement.id = "button";
            buttonElement.innerText = button.text;
            buttonElement.addEventListener("click", button.callback);

            buttonsBlock.appendChild(buttonElement);
        }
    }

    show() {
        this.update();
        this.element.style.visibility = "visible";
    }

    hide() {
        this.element.style.visibility = "hidden";
    }

    close() {
        this.hide();
        this.clear();
    }

    clear() {
        this.title = "";
        this.message = "";
        this.buttons = [];
    }

    addButton(text, callback) {
        this.buttons.push({text, callback});
    }
}