import TaskComponent from '../components/task.component.js'

export var TaskService = {
    list: [],

    add: function(task) {
        this.list.push(task);

        this.export();
    },

    remove: function(taskID) {
        for (let i = 0; this.list.length; ++i) {
            if (this.list[i].id == taskID) {
                this.list.splice(i, 1);
            }
        }

        this.export();
    },

    indexOf: function(taskID) {
        for (let i = 0; this.list.length; ++i) {
            if (this.list[i].id == taskID) {
                return i;
            }
        }

        return -1;
    },

    sortByPriority: function() {
        this.list = this.list.sort((task1, task2)=> {
            if (task1.priority < task2.priority) {
                return 1;
            } else if(task1.priority > task2.priority) {
                return -1;
            }

            return 0;
        });
    },

    import: function() {
        if (window.localStorage.hasOwnProperty("tasks")) {
            const str = window.localStorage.getItem("tasks");
            const arr = JSON.parse(str);

            for (let i = 0; i < arr.length; ++i) {
                const json = arr[i];
                let task = new TaskComponent();
                task.fromJson(json);

                this.list.push(task);
            }
        }

        this.sortByPriority();
    },

    export: function() {
        var items = [];
        for (let i = 0; i < this.list.length; ++i) {
            const task = this.list[i];

            if (task.category != TaskComponent.Category.Trash) {
                const json = task.toJson();
                items.push(json);
            }
        }

        const str = JSON.stringify(items);
        window.localStorage.setItem("tasks", str);
    }
}

TaskService.import();