/*----------------[Imports]----------------*/

import { Utils } from './utils.js'

//Services
import {TaskService} from './services/task.service'

//Components
import TaskComponent from './components/task.component.js'
import TaskContainerComponent from './components/task.container.component.js'
import PopUpComponent from './components/popup.component.js'

//Modules
import Drap from './modules/drap.module.js'

/*----------------[Constants]----------------*/

const ClickType = {
    None: 0,
    DeleteTask: 1,
    EditTask: 2
}

const CategoryTitles = {
    0: "Pending",
    1: "Selected for Work",
    2: "In Progress",
    3: "Done"
};

/*----------------[Members]----------------*/

//Modules
let moduleDrap = new Drap();
moduleDrap.onDrop = onTaskMove;

//Elements
let elementContainers;

let elementAddTaskButton;
let elementEditTaskButton;
let elementDeleteTaskButton;
let elementSortTasksButton;

let taskContainers = {};

//Templates
let taskEditFormTemplate;

//Pop-up
let taskPopup = new PopUpComponent();
let acceptPopup = new PopUpComponent();
let errorPopup = new PopUpComponent();

let clickType = ClickType.None;

/*----------------[Init]----------------*/

window.onload = function() {
    //Tasks
    elementContainers = document.querySelector("#containers");

    //Toolbar
    elementAddTaskButton = document.querySelector("#add-task-button");
    elementEditTaskButton = document.querySelector("#edit-task-button");
    elementDeleteTaskButton = document.querySelector("#delete-task-button");
    elementSortTasksButton = document.querySelector("#sort-tasks-button");

    //Templates
    taskEditFormTemplate = document.querySelector("#task-edit-form-template");

    //Events
    elementAddTaskButton.onclick = onAddTask;
    elementEditTaskButton.onclick = onEditTask;
    elementDeleteTaskButton.onclick = onDeleteTask;
    elementSortTasksButton.onclick = onSortTasks;
    
    //Initialization
    initTaskContainers(false);
    initTasks(false);
}

function initTaskContainers(recreate = true) {
    for (let category in TaskComponent.Category) {
        const index = TaskComponent.Category[category];
        const title = CategoryTitles[index];

        if (!taskContainers[index]) {
            taskContainers[index] = new TaskContainerComponent(index, title);
        }

        drawTaskContainer(taskContainers[index], recreate);
    }
}

function initTasks(recreate = true) {
    for (let i = 0; i < TaskService.list.length; ++i) {
        const task = TaskService.list[i];
        drawTask(task, recreate);
    }
}

/*----------------[Task Handler]----------------*/

function createTask() {
    taskPopup.clear();

    taskPopup.title = "Create New Task";
    taskPopup.message = taskEditFormTemplate.innerHTML;
    
    taskPopup.addButton("Cancel", ()=> {
        taskPopup.close();
    });

    taskPopup.addButton("Create", ()=> {
        const elementTaskTitle = taskPopup.element.querySelector("#message div#task-title input");
        const elementTaskDescription = taskPopup.element.querySelector("#message div#task-description textarea");
        const elementTaskPriority = taskPopup.element.querySelector("#message div#task-priority input");

        const title = elementTaskTitle.value;
        const description = elementTaskDescription.value;
        const priority = parseInt(elementTaskPriority.value);

        //Check if all fields are filled correctly
        const isValid = checkTaskEditFields(title, description, priority);
        if (isValid) {
            //Create new task
            let task = new TaskComponent(title, description, priority);

            //Add task to service
            TaskService.add(task);

            //Reinitialize tasks at html
            initTasks();

            //Close popup
            taskPopup.close();
        }
    });
    
    taskPopup.show();
}

function editTask(taskID) {
    const index = TaskService.indexOf(taskID);
    if (index < 0) {
        console.error("Task " + taskID + " is not found");
        return;
    }

    let task = TaskService.list[index];

    taskPopup.clear();

    taskPopup.title = "Edit Task";
    taskPopup.message = taskEditFormTemplate.innerHTML;
    
    taskPopup.addButton("Cancel", ()=> {
        taskPopup.close();
    });

    taskPopup.addButton("Apply", ()=> {
        const title = elementTaskTitle.value;
        const description = elementTaskDescription.value;
        const priority = parseInt(elementTaskPriority.value);

        //Check if all fields are filled correctly
        const isValid = checkTaskEditFields(title, description, priority);
        if (isValid) {
            //Change task properties
            task.title = title;
            task.description = description;
            task.priority = priority;

            //Update task html
            task.update();

            //Close popup
            taskPopup.close();
        }
    });
    
    taskPopup.show();

    let elementTaskTitle = taskPopup.element.querySelector("#message div#task-title input");
    let elementTaskDescription = taskPopup.element.querySelector("#message div#task-description textarea");
    let elementTaskPriority = taskPopup.element.querySelector("#message div#task-priority input");

    elementTaskTitle.value = task.title;
    elementTaskDescription.value = task.description;
    elementTaskPriority.value = task.priority;
}

function deleteTask(taskID) {
    showAcceptPopup("Delete task", "Are you sure you want to delete this task?", (result)=> {
        if (result) {
            const index = TaskService.indexOf(taskID); 
            TaskService.list[index].element.remove();
            TaskService.list.splice(index, 1);
            TaskService.export();
        }
    });
}

function sortTasks() {
    TaskService.sortByPriority();
    initTasks();
}

/*----------------[Checks]----------------*/

function checkTaskEditFields(title, description, priority) {
    if (!title || !description || !priority) {
        showErrorPopup("Please fill in all of the required fields.");
    } else if (title.length < TaskComponent.MinTitleLength) {
        showErrorPopup("The title should contain " + TaskComponent.MinTitleLength + " or more characters");
    } else if (title.length > TaskComponent.MaxTitleLength) {
        showErrorPopup("The title should not exceed " + TaskComponent.MaxTitleLength + " characters");
    } else if (description.length < TaskComponent.MinDescriptionLength) {
        showErrorPopup("The description should contain " + TaskComponent.MinDescriptionLength + " or more characters");
    } else if (description.length > TaskComponent.MaxDescriptionLength) {
        showErrorPopup("The description should not exceed " + TaskComponent.MaxDescriptionLength + " characters");
    } else if (priority < 0) {
        showErrorPopup("The priority should be 0 or higher");
    } else if (priority > 10) {
        showErrorPopup("The priority should be 10 or less");
    } else {
        return true;
    }

    return false;
}

/*----------------[Draw]----------------*/

function drawTask(task, recreate = true) {
    if (elementContainers.contains(task.element)) {
        if (recreate) {
            task.element.remove();
            taskContainers[task.category].element.appendChild(task.element);
        }
    } else {
        taskContainers[task.category].element.appendChild(task.element);
    }

    //Update html for component
    task.update();

    //Reinitialize drap module
    moduleDrap.init();
}

function drawTaskContainer(container, recreate = true) {
    if (elementContainers.contains(container.element)) {
        if (recreate) {
            container.element.remove();
            elementContainers.appendChild(container.element);    
        }
    } else {
        elementContainers.appendChild(container.element);
    }

    //Update html for component
    container.update();
}

/*----------------[Pop-up]----------------*/

function showAcceptPopup(title, message, callback) {
    acceptPopup.clear();

    acceptPopup.title = title;
    acceptPopup.message = message;
    
    acceptPopup.addButton("Decline", ()=> {
        acceptPopup.close();
        callback(false);
    });

    acceptPopup.addButton("Accept", ()=> {
        acceptPopup.close();
        callback(true);
    });
    
    acceptPopup.show();
}

function showErrorPopup(message) {
    errorPopup.clear();

    errorPopup.title = "Error";
    errorPopup.message = message;

    errorPopup.addButton("Ok", ()=> {
        errorPopup.close();
    });

    errorPopup.show();
}

/*----------------[Events]----------------*/

function onAddTask() {
    createTask();
}

function onEditTask() {
    setTimeout(()=> {
        clickType = ClickType.EditTask;
        document.addEventListener("mousedown", onDocumentMouseDown);    
    }, 1);
}

function onDeleteTask() {
    setTimeout(()=> {
        clickType = ClickType.DeleteTask;
        document.addEventListener("mousedown", onDocumentMouseDown);    
    }, 1);
}

function onSortTasks() {
    sortTasks();
}

function onTaskMove(element, container) {
    const taskID = element.getAttribute("task-id");
    
    let result = false;
    if (container.id == elementDeleteTaskButton.id) {
        deleteTask(taskID);
        return false;
    } else if (container.id == elementEditTaskButton.id) {
        editTask(taskID);
        return false;
    } else {
        const index = TaskService.indexOf(taskID);
        const category = parseInt(container.getAttribute("task-category"));
        result = TaskService.list[index].moveTo(category);
        
        TaskService.export();
    }

    return result;
}

function onDocumentMouseDown(event) {
    if (clickType == ClickType.DeleteTask || clickType == ClickType.EditTask) {
        let element = event.target;
        while(element) {
            if (element.tagName == "TASK") {
                const taskID = element.getAttribute("task-id");

                if (clickType == ClickType.DeleteTask) {
                    deleteTask(taskID);
                } else if (clickType == ClickType.EditTask) {
                    editTask(taskID);
                }

                break;
            }

            element = element.parentNode;
        }
    }

    clickType = ClickType.None;
    document.removeEventListener("mousedown", onDocumentMouseDown);
}