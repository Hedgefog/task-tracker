export default class Drap {
    constructor() {
        this.target = null;

        this.cursor = {
            x: 0,
            y: 0
        };

        this.offset = {
            x: 0,
            y: 0
        }

        this.init();

        this.onDrop = null;

        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
        this.onMouseMove =  this.onMouseMove.bind(this);
    }

    init() {
        const draggables = document.querySelectorAll(".drap-draggable");
        [].forEach.call(draggables, (draggable) => {
            draggable.style.zIndex = 1024;
            draggable.addEventListener("mousedown", this.onMouseDown);
        });
    }

    drag(element) {
        if (this.target) {
            this.release();
        }

        const rect = element.getBoundingClientRect();

        this.target = element;
        this.target.style.position = "absolute";
        this.target.style.width = rect.width;

        document.addEventListener("mousemove", this.onMouseMove);
    }

    release() {
        if (!this.target) {
            return;
        }

        this.drop();

        document.removeEventListener("mousemove", this.onMouseMove);
        document.removeEventListener("mouseup", this.onMouseUp);

        this.target = null;
    }

    drop() {
        const containers = [].slice.call(document.querySelectorAll(".drap-drop-container"));
        for (let i = 0; i < containers.length; ++i) {
            const container = containers[i];

            if (container == this.target.parentElement) {
                continue;
            }

            const containerRect = container.getBoundingClientRect();
            if (this.cursor.x >= containerRect.left && this.cursor.x <= containerRect.right
                && this.cursor.y >= containerRect.top && this.cursor.y <= containerRect.bottom) {

                var acceptDrop = (!this.onDrop);
                if (!acceptDrop) {
                    acceptDrop = this.onDrop(this.target, container);
                }

                if (acceptDrop != false) {
                    container.appendChild(this.target);
                }

                break;
            }
        }

        this.target.style.position = "";
        this.target.style.left = "";
        this.target.style.top = "";
        this.target.style.width = "";
    }

    onMouseDown(event) {
        if (this.target) {
            return;
        }
        
        this.offset = {
            x: event.offsetX,
            y: event.offsetY,
        }

        this.cursor = {
            x : event.clientX,
            y : event.clientY
        }

        if(event.target.classList.contains("drap-draggable")) {
            this.drag(event.currentTarget);
            document.addEventListener("mouseup", this.onMouseUp);
        }
    }

    onMouseUp(event) {
        this.release();
    }

    onMouseMove(event) {
        const rect = this.target.getBoundingClientRect();

        this.cursor = {
            x : event.clientX,
            y : event.clientY
        }

        if (this.target) {
            this.target.style.left = (this.cursor.x - this.offset.x + pageXOffset) + "px";
            this.target.style.top = (this.cursor.y - this.offset.y + pageYOffset) + "px";
        }
    }
}