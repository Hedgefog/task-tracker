export var CreateComponent = function(data) {
    let component = class {
        constructor() {
            this.element = document.createElement(this.constructor.selector);
        }

        update() {
            let template = this.constructor.template;

            for (let member in this) {
                const field = "{" + member + "}";
                template = template.replace(field, this[member]);
            }
            
            this.element.innerHTML = template;
        }
    }

    for (let key in data) {
        component[key] = data[key];
    }

    return component;
}